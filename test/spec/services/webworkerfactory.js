'use strict';

describe('Service: webworkerFactory', function () {

  // load the service's module
  beforeEach(module('angularWebworkerApp'));

  // instantiate service
  var webworkerFactory;
  beforeEach(inject(function (_webworkerFactory_) {
    webworkerFactory = _webworkerFactory_;
  }));

  it('should do something', function () {
    expect(!!webworkerFactory).toBe(true);
  });

});
