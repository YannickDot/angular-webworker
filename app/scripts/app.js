'use strict';

/**
 * @ngdoc overview
 * @name angularWebworkerApp
 * @description
 * # angularWebworkerApp
 *
 * Main module of the application.
 */
angular
  .module('angularWebworkerApp', []);
