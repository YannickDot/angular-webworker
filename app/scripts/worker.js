'use strict';

importScripts('../bower_components/atomic/dist/atomic.min.js');

// this.addEventListener('message', function(e) {
// 	this.postMessage('Received : ' + e.data);
// }, false);

// this.addEventListener('message', function(e) {
// 	this.postMessage('Received : ' + e.data);
// }, false);

var response;

self.addEventListener('message', function(e) {
	atomic.get(e.data)
	  .success(function (data, xhr) {
	  	response = data;
	  	self.postMessage(JSON.stringify(data));
	  })
	  .error(function (data, xhr) {
	  	self.postMessage('ERR');
	  });

}, false);