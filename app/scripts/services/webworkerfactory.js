'use strict';

/**
 * @ngdoc service
 * @name angularWebworkerApp.webworkerFactory
 * @description
 * # webworkerFactory
 * Factory in the angularWebworkerApp.
 */
angular.module('angularWebworkerApp')
  .factory('webworkerFactory', function ($rootScope) {
    var exports = {},
        workerFile = 'scripts/worker.js',
        workerInstance = new Worker(workerFile),
        result = [], 
        promise;


        workerInstance.addEventListener('message', function(e) {
          result.push(e.data);
          $rootScope.$digest();
        });

        exports.post = function (message) {
          workerInstance.postMessage(message);
        };

        exports.setWorkerFile = function (path) {
          workerFile = path;

          if(workerInstance) {
            workerInstance.terminate();
            workerInstance = new Worker(path);
          } else {
            workerInstance = new Worker(path);
          }
        };

        exports.stopWorker = function() {
          workerInstance.terminate();
        };

        exports.promiseAnswer = function () {
          return promise;
        };

        exports.result = result;

        return exports;
  });
