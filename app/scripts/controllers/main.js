'use strict';

/**
 * @ngdoc function
 * @name angularWebworkerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularWebworkerApp
 */
angular.module('angularWebworkerApp')
  .controller('MainCtrl', function ($scope, webworkerFactory) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    var url = 'https://public.opencpu.org/ocpu/library/';
    var url2 = 'http://ip.jsontest.com/';
    var url3 = 'http://date.jsontest.com';
    var url4 = 'http://headers.jsontest.com/';
    var url5 = '';

    var arr = [url4, url2, url3, url];
    var msg = 'yipeeeeee';

    $scope.post = function(index) {
    	webworkerFactory.post(arr[index]);
    };

    $scope.halt = function() {
    	webworkerFactory.stopWorker();
    };

    $scope.result = function() {
    	console.log(JSON.parse(webworkerFactory.result));
    };

    $scope.res = webworkerFactory.result;

    $scope.$watch('res', function(newVal, oldVal) {
		console.log(newVal, oldVal);
	});

  });
